package com.practice.cloud;

import com.practice.cloud.student.Student;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class StudentRepository {


    public List<Student> students() {
        return Arrays.asList(new Student(1, "shaik", 516),
                new Student(2, "sudheer", 490),
                new Student(3, "arif", 500),
                new Student(4, "ravi", 900));
    }

}
