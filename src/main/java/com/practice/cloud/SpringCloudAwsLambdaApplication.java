package com.practice.cloud;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.practice.cloud.student.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.CollationElementIterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@SpringBootApplication
public class SpringCloudAwsLambdaApplication {


    @Autowired
    StudentRepository studentRepository;


    @Bean
    public Supplier<List<Student>> students() {
        return () -> studentRepository.students();
    }

    @Bean
    public Function<APIGatewayProxyRequestEvent, List<Student>> getStudent() {
        return (requestEvent) -> studentRepository.students().stream().filter((student)
                -> student.getName().equalsIgnoreCase(requestEvent.getQueryStringParameters()
                .get("orderName"))).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudAwsLambdaApplication.class, args);
    }

}
